#ifndef MU_LOG_H_
#define MU_LOG_H_

#include <mutex>
#include <iostream>
#include <ostream>
#include <cstdint>
#include <cstdarg>
#include <cstring>
#include <cctype>
#include <chrono>
#include <memory>
#include <ctime>
#include <vector>
#include <sstream>

#define MU_LOG_HEAD_DEBUG "DEBUG"
#define MU_LOG_HEAD_INFO "INFO"
#define MU_LOG_HEAD_WARN "WARNING"
#define MU_LOG_HEAD_ERR "ERROR"
#define MU_LOG_HEAD_CRIT "CRITICAL"
#define MU_LOG_HEAD_LOG_ERROR "???"

#define ANSI_ESC "\033["
#define ANSI_DLM ";"
#define ANSI_END "m"
#define ANSI_RST "0"
#define ANSI_BOL "1"
#define ANSI_UND "4"
#define ANSI_COLOR_CYAN "36"
#define ANSI_COLOR_YELLOW "33"
#define ANSI_COLOR_RED "31"
#define ANSI_COLOR_WHITE "37"
#define ANSI_BKG_COLOR_RED "41"

#define ANSI_RESET_ESCSEQ	(ANSI_ESC \
							 ANSI_RST \
							 ANSI_END)

#define ANSI_DEBUG_ESCSEQ	(ANSI_ESC \
							 ANSI_COLOR_CYAN \
							 ANSI_DLM \
							 ANSI_BOL \
							 ANSI_END)

#define ANSI_WARN_ESCSEQ	(ANSI_ESC \
							 ANSI_COLOR_YELLOW \
							 ANSI_DLM \
							 ANSI_BOL \
							 ANSI_END)

#define ANSI_ERR_ESCSEQ		(ANSI_ESC \
							 ANSI_COLOR_RED \
							 ANSI_DLM \
							 ANSI_BOL \
							 ANSI_END)

#define ANSI_ABORT_ESCSEQ	(ANSI_ESC \
							 ANSI_COLOR_WHITE \
							 ANSI_DLM \
							 ANSI_BOL \
							 ANSI_DLM \
							 ANSI_BKG_COLOR_RED \
							 ANSI_END)

namespace boyan {


	namespace mutables {

		typedef uint32_t log_bitflag_t;

		// reset
		static const log_bitflag_t bit_rst = 0x00;

		// logging level
		static const log_bitflag_t bit_abrt = 0x01;				// 0b0000 0001
		static const log_bitflag_t bit_err = 0x02 | bit_abrt;		// 0b0000 0011
		static const log_bitflag_t bit_warn = 0x04 | bit_err;		// 0b0000 0111
		static const log_bitflag_t bit_info = 0x08 | bit_info;		// 0b0000 1111
		static const log_bitflag_t bit_debug = 0x10 | bit_debug;	// 0b0001 1111		

		// advanced options
		static const log_bitflag_t bit_noansi = 0x20;				// 0b0010 0000
		static const log_bitflag_t bit_noflush = 0x40;				// 0b0100 0000
		static const log_bitflag_t bit_notime = 0x80;				// 0b1000 0000
		static const log_bitflag_t bit_noprintsig = 0x100;			// etc... we have 32 bits to fill
		static const log_bitflag_t bit_newline = 0x200;

		class log {

		private:

			enum log_level_e {
				MU_LOG_LVL_DEBUG = 0,
				MU_LOG_LVL_INFO,
				MU_LOG_LVL_WARN,
				MU_LOG_LVL_ERR,
				MU_LOG_LVL_CRIT
			};

			struct spvchar_buffer_s {
				/// this is a disgusting hack,
				/// however it is guaranteed that the vector
				/// is storing its data in a contiguous
				/// block of memory, so this does not
				/// need an immediate rework
				std::shared_ptr<std::vector<char>> sptr;
				int rbuflen;
			};

			static std::mutex _mu_os;
			static std::mutex _mu_reinit;
			static std::ostream * _os;
			static log_bitflag_t _bit_fl;

			static void _vconstrbuf(
				spvchar_buffer_s& _spvcharbuf,
				const char * _fmt,
				...
			);

			static void _logsigprint(
				spvchar_buffer_s& _spvcharbuf,
				const char * _seq,
				const char * _head,
				const char * _end = ANSI_RESET_ESCSEQ
			);

			static void _logtmsprint(
				spvchar_buffer_s& _rbuf,
				std::tm const _syst_time
			);

			static void _constrbuf(
				spvchar_buffer_s& _rbuf,
				const char * fmt,
				va_list _args
			);

			static void _oslogpush(
				const char * fmt,
				va_list _args,
				log_level_e _mulevel
			);

			static const char* _slsig(
				log_level_e _mulevel
			);

			static const char* _sansic(
				log_level_e _mulevel
			);

		public:

			log() = delete; // no constructor
			~log() = delete; // no destructor
			log(const log&) = delete; // no copy
			log& operator= (const log&) = delete; // no selfies

			static void reinitialize(
				std::ostream& os,
				log_bitflag_t flags
			);

			static void reinitialize(
				std::ostream& os
			);

			static void D(
				const char * fmt,
				...
			);

			static void I(
				const char * fmt,
				...
			);

			static void W(
				const char * fmt,
				...
			);

			static void E(
				const char * fmt,
				...
			);

			static void X(
				const char * fmt,
				...
			);
		};
	}

}

#endif // MU_LOG_H_
