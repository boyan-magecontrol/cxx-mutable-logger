#include "mutablelog.h"

// private static default
std::ostream * boyan::mutables::log::_os = &std::cout;
// private static default
boyan::mutables::log_bitflag_t boyan::mutables::log::_bit_fl = boyan::mutables::bit_debug;
// private static default
std::mutex boyan::mutables::log::_mu_os;
// private static default
std::mutex boyan::mutables::log::_mu_reinit;

/// doc
void boyan::mutables::log::reinitialize(
	std::ostream& os,
	boyan::mutables::log_bitflag_t bit_fl
) {

	std::unique_lock<std::mutex> _u_lock_params =
		std::unique_lock<std::mutex>(boyan::mutables::log::_mu_reinit, std::defer_lock);

	_u_lock_params.lock();
	boyan::mutables::log::_os = &os;
	boyan::mutables::log::_bit_fl = bit_fl;
	_u_lock_params.unlock();

}

void boyan::mutables::log::reinitialize(
	std::ostream& os
) {
	
	std::unique_lock<std::mutex> _u_lock_params =
		std::unique_lock<std::mutex>(boyan::mutables::log::_mu_reinit, std::defer_lock);

	_u_lock_params.lock();
	boyan::mutables::log::_os = &os;
	_u_lock_params.unlock();
}

/// doc
/// does not call va_end(_args)
void boyan::mutables::log::_constrbuf(
	boyan::mutables::log::spvchar_buffer_s& _rbuf,
	const char * fmt,
	va_list _args
) {
#if defined (_WIN32) || (_WIN64)
	_rbuf.rbuflen = _vscprintf(fmt, _args);
#else
	_rbuf.rbuflen = std::vsprintf(NULL, fmt, _args);
#endif
	std::shared_ptr<std::vector<char>> _sp = std::make_shared<std::vector<char>>(_rbuf.rbuflen + 1);
	// make a strong reference to the pointer
	_rbuf.sptr = _sp;

	// Terminate manually
	// Some implementations of C++ do not bother to terminate through
	// sprintf derivatives
	_sp.get()->at(_rbuf.rbuflen) = '\0';

	if (_rbuf.sptr != nullptr) {
		// _sp.get()->data() is the equivalent of
		// &(*_sp.get())[0], which can only be used
		// after C++11. This is a nasty hack and relies
		// on the guarantee that vector data is stored
		// in contiguous blocks of memory.
		std::vsnprintf(_sp.get()->data(), _rbuf.rbuflen + 1, fmt, _args);
	}
}

/// doc
void boyan::mutables::log::_vconstrbuf(
	boyan::mutables::log::spvchar_buffer_s& _rbuf,
	const char * fmt,
	...
) {
	va_list _args;
	va_start(_args, fmt);
	boyan::mutables::log::_constrbuf(_rbuf, fmt, _args);

	// must be manually called
	va_end(_args);
}

/// doc
void boyan::mutables::log::_logsigprint(
	boyan::mutables::log::spvchar_buffer_s& _rbuf,
	const char * seq,
	const char * head,
	const char * end
) {
	boyan::mutables::log::_vconstrbuf(_rbuf,
		"[%s%s%s]",
		seq,
		head,
		end);
}

/// doc
void boyan::mutables::log::_logtmsprint(
	boyan::mutables::log::spvchar_buffer_s& _rbuf,
	std::tm const _syst_time
) {
	boyan::mutables::log::_vconstrbuf(_rbuf,
		"(%d/%d/%d; %d:%d:%d)",
		_syst_time.tm_year + 1900,
		_syst_time.tm_mon + 1,
		_syst_time.tm_mday,
		_syst_time.tm_hour,
		_syst_time.tm_min,
		_syst_time.tm_sec);
}

const char* boyan::mutables::log::_slsig(
	boyan::mutables::log::log_level_e _mulevel
) {
	switch (_mulevel) {
	case MU_LOG_LVL_DEBUG:
		return MU_LOG_HEAD_DEBUG;
		break;
	case MU_LOG_LVL_INFO:
		return MU_LOG_HEAD_INFO;
		break;
	case MU_LOG_LVL_WARN:
		return MU_LOG_HEAD_WARN;
		break;
	case MU_LOG_LVL_ERR:
		return MU_LOG_HEAD_ERR;
		break;
	case MU_LOG_LVL_CRIT:
		return MU_LOG_HEAD_CRIT;
		break;
	default:
		return MU_LOG_HEAD_LOG_ERROR;
		break;
	}
}

const char* boyan::mutables::log::_sansic(
	boyan::mutables::log::log_level_e _mulevel
) {
	switch (_mulevel) {
	case MU_LOG_LVL_DEBUG:
		return ANSI_DEBUG_ESCSEQ;
		break;
	case MU_LOG_LVL_INFO:
		return ANSI_RESET_ESCSEQ;
		break;
	case MU_LOG_LVL_WARN:
		return ANSI_WARN_ESCSEQ;
		break;
	case MU_LOG_LVL_ERR:
		return ANSI_ERR_ESCSEQ;
		break;
	case MU_LOG_LVL_CRIT:
		return ANSI_ABORT_ESCSEQ;
		break;
	default:
		return ANSI_ERR_ESCSEQ;
		break;
	}
}

/// doc
void boyan::mutables::log::_oslogpush(
	const char * fmt,
	va_list _args,
	boyan::mutables::log::log_level_e _mulevel
) {

	// obtain time
	// first thing, in order to be as accurate as
	// possible
	std::chrono::time_point<std::chrono::system_clock> _time =
		std::chrono::system_clock::now();

	// DECLARATIONS

	// declare local flags,
	// so that settings parameters
	// can be read only once, resulting in a
	// shorter lock
	boyan::mutables::log_bitflag_t _loc_bitfl;

	std::stringstream _ss;

	// obtain unique locks
	std::unique_lock<std::mutex> _u_lock_params =
		std::unique_lock<std::mutex>(boyan::mutables::log::_mu_reinit, std::defer_lock);

	std::unique_lock<std::mutex> _u_lock_stream =
		std::unique_lock<std::mutex>(boyan::mutables::log::_mu_os, std::defer_lock);

	// check settings
	// lock parameter mutex so logging settings
	// can not be changed while checking them
	_u_lock_params.lock();
	_loc_bitfl = boyan::mutables::log::_bit_fl;
	_u_lock_params.unlock();


	// if needed, construct signature and output
	// to stringstream
	if (!(_loc_bitfl & boyan::mutables::bit_noprintsig)) {
		// decide on signature
		boyan::mutables::log::spvchar_buffer_s _seq_buf;
		// check if needs to be ANSI
		// colorful and construct the signature
		if (!(_loc_bitfl & boyan::mutables::bit_noansi)) {
			boyan::mutables::log::_logsigprint(_seq_buf, boyan::mutables::log::_sansic(_mulevel), boyan::mutables::log::_slsig(_mulevel), ANSI_RESET_ESCSEQ);
			_ss << _seq_buf.sptr.get()->data();
		}
		else {
			boyan::mutables::log::_logsigprint(_seq_buf, "", boyan::mutables::log::_slsig(_mulevel), "");
			_ss << _seq_buf.sptr.get()->data();
		}
	}

	// if needed, construct timestamp and output
	// to stringstream
	if (!(_loc_bitfl & boyan::mutables::bit_notime)) {
		// convert to std::tm
		std::time_t _syst_time = (std::chrono::system_clock::to_time_t(_time));
		std::tm _syst_cal_time;
		// thread safe implementations of std::localtime
#if defined (_WIN32) || (_WIN64)
		localtime_s(&_syst_cal_time, &_syst_time);
#else
		localtime_r(&_syst_time, &_syst_cal_time);
#endif
		boyan::mutables::log::spvchar_buffer_s _tms_buf;
		boyan::mutables::log::_logtmsprint(_tms_buf, _syst_cal_time);
		_ss << _tms_buf.sptr.get()->data();
	}

	// create the message
	boyan::mutables::log::spvchar_buffer_s _msg_buf;

	// construct message
	boyan::mutables::log::_constrbuf(_msg_buf, fmt, _args);

	// if signature or timestamp is enabled, we need a ":"
	_ss << ((_loc_bitfl & boyan::mutables::bit_noprintsig) && (_loc_bitfl & boyan::mutables::bit_notime) ? "" : ": ");
	// check if new line between message is required
	_ss << ((_loc_bitfl & boyan::mutables::bit_newline) ? "\n\t" : "");
	_ss << _msg_buf.sptr.get()->data();

	// time to put the data out the actual
	// output stream
	_u_lock_stream.lock();
	// check if we need to flush the stream
	if (!(_loc_bitfl & boyan::mutables::bit_noflush)) {
		(*_os) << _ss.str() << std::endl;
	}
	else {
		(*_os) << _ss.str() << "\r\n";
	}
	// unlock the stream
	_u_lock_stream.unlock();
}

/// doc
void boyan::mutables::log::D(
	const char * fmt,
	...
) {
	va_list _args;
	// convert varargs to va_list
	va_start(_args, fmt);
	boyan::mutables::log::_oslogpush(fmt, _args, MU_LOG_LVL_DEBUG);
	// must be manually called
	va_end(_args);
}

/// doc
void boyan::mutables::log::I(
	const char * fmt,
	...
) {
	va_list _args;
	// convert varargs to va_list
	va_start(_args, fmt);
	boyan::mutables::log::_oslogpush(fmt, _args, MU_LOG_LVL_INFO);
	// must be manually called
	va_end(_args);
}

/// doc
void boyan::mutables::log::W(
	const char * fmt,
	...
) {
	va_list _args;
	// convert varargs to va_list
	va_start(_args, fmt);
	boyan::mutables::log::_oslogpush(fmt, _args, MU_LOG_LVL_WARN);
	// must be manually called
	va_end(_args);
}

/// doc
void boyan::mutables::log::E(
	const char * fmt,
	...
) {
	va_list _args;
	// convert varargs to va_list
	va_start(_args, fmt);
	boyan::mutables::log::_oslogpush(fmt, _args, MU_LOG_LVL_ERR);
	// must be manually called
	va_end(_args);
}

/// doc
void boyan::mutables::log::X(
	const char * fmt,
	...
) {
	va_list _args;
	// convert varargs to va_list
	va_start(_args, fmt);
	boyan::mutables::log::_oslogpush(fmt, _args, MU_LOG_LVL_CRIT);
	// must be manually called
	va_end(_args);
}