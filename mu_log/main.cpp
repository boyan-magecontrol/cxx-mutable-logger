#include "mutablelog.h"

int main(int argc, char ** argv) {

	using namespace boyan;

	mutables::log::reinitialize(std::cout, mutables::bit_debug | mutables::bit_newline);

	mutables::log::D("This is a debug message, from line %d", 136);
	mutables::log::I("This is an informational message");
	mutables::log::W("This is a warning message from %s", "THREAD_ID ??3");
	mutables::log::E("Exception thrown by std::array");
	mutables::log::X("This program will now exit");

	system("pause");
}