[![Build Status](https://travis-ci.org/bochko/CXX-Mutable-Logger.svg?branch=master)](https://travis-ci.org/bochko/CXX-Mutable-Logger)


## C++ Mutable Logger

### A static implementation of a thread-safe, output stream agnostic message logger.

--- 
